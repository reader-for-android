/**
 * This source code and any associated intellectual 
 * property is owned by Deeveecube Inc.
 *
 * 2010-2010
 */
package com.deeveecube.reader.filesystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.deeveecube.reader.R;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This activity provides file system navigation functionality.
 * 
 * @author Dmitry Zhuk
 */
public class FileSystemActivity extends ListActivity {
	/** Describes file to be shown on the screen */
	private class FileDescriptor {
		private FileDescriptor(String name, String info, File file) {
			this.name = name;
			this.info = info;
			this.file = file;
		}

		private String name;
		private String info;
		private File file;
	}

	/** Custom adapter for ListView based on {@link ArrayAdapter} */
	private class FileAdapter extends ArrayAdapter<FileDescriptor> {
		/** @see ArrayAdapter#ArrayAdapter(Context, int, List) */
		public FileAdapter(Context context, int textViewResourceId,
				List<FileDescriptor> items) {
			super(context, textViewResourceId, items);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = LayoutInflater.class
						.cast(FileSystemActivity.this
								.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
				v = vi.inflate(R.layout.file, null);
			}
			FileDescriptor fd = this.getItem(position);
			if (fd != null) {
				TextView fn = TextView.class
						.cast(v.findViewById(R.id.filename));
				TextView fi = TextView.class
						.cast(v.findViewById(R.id.fileinfo));
				ImageView icon = ImageView.class
						.cast(v.findViewById(R.id.icon));
				if (fn != null) {
					fn.setText(fd.name);
				}
				if (fi != null) {
					fi.setText(fd.info);
				}
				if (fd.file.isDirectory()) {
					icon.setImageResource(R.drawable.ic_menu_archive);
				} else {
					icon.setImageResource(R.drawable.ic_menu_compose);
				}
			}
			return v;
		}
	}

	/** List of {@link FileDescriptor} items to display */
	private List<FileDescriptor> items;
	/** Current folder to display contents for */
	private File current;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.filesystem);
		final String currentFileName = savedInstanceState == null ? null
				: savedInstanceState.getString("current");
		this.current = new File(currentFileName == null ? "/sdcard"
				: currentFileName);
		this.items = new ArrayList<FileDescriptor>();
		this.setListAdapter(new FileAdapter(this, R.layout.file, this.items));
		this.updateView();
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
		final String currentFileName = state.getString("current");
		this.current = new File(currentFileName == null ? "/sdcard"
				: currentFileName);
		this.updateView();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("current", this.current.getAbsolutePath());
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		FileDescriptor fd = this.items.get(position);
		if (fd.file.isDirectory()) {
			try {
				this.current = new File(this.current.getAbsolutePath()
						+ File.separator + fd.file.getName())
						.getCanonicalFile();
			} catch (IOException e) {
				Log.w(getLocalClassName(), e);
			}
			this.updateView();
			// XXX l.scrollTo(0, 0);
		}
	}

	@Override
	public void onBackPressed() {
		if (!"/".equals(FileSystemActivity.this.current.getPath())) {
			try {
				this.current = new File(this.current.getAbsolutePath()
						+ File.separator + "..").getCanonicalFile();
			} catch (IOException e) {
				Log.w(getLocalClassName(), e);
			}
			this.updateView();
		} else {
			super.onBackPressed();
		}
	}

	private void updateView() {
		final ProgressDialog pd = ProgressDialog.show(FileSystemActivity.this,
				"Please wait", "Loading list of files", true);
		final FileAdapter fa = FileAdapter.class.cast(this.getListAdapter());
		this.items.clear();
		fa.notifyDataSetChanged();
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (!"/".equals(FileSystemActivity.this.current.getPath())) {
					FileSystemActivity.this.items.add(new FileDescriptor("..",
							"<parent>", new File("..")));
				}
				File[] files = FileSystemActivity.this.current.listFiles();
				if (files != null) {
					for (File file : files) {
						if (file.isDirectory()) {
							String[] list = file.list();
							FileSystemActivity.this.items.add(new FileDescriptor(
									file.getName(), String
											.valueOf(list == null ? 0
													: list.length)
											+ " files", file));
						} else {
							final int block = 1024;
							final String[] units = { "B", "KB", "MB" };
							long length = file.length();
							int iteration = 0;
							while (length > block && iteration < units.length) {
								length /= block;
								++iteration;
							}
							FileSystemActivity.this.items.add(new FileDescriptor(
									file.getName(), String.valueOf(length)
											+ " " + units[iteration], file));
						}
					}
				}
				FileSystemActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						fa.sort(new Comparator<FileDescriptor>() {
							@Override
							public int compare(FileDescriptor object1,
									FileDescriptor object2) {
								if (object1.file.isDirectory()
										&& !object2.file.isDirectory()) {
									return -1;
								} else if (!object1.file.isDirectory()
										&& object2.file.isDirectory()) {
									return 1;
								}
								return object1.name.compareTo(object2.name);
							}

						});
						fa.notifyDataSetChanged();
						pd.dismiss();
					}
				});
			}
		}, "ListFiles").start();
	}
}
