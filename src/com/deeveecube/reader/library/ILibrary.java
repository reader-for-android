/**
 * This source code and any associated intellectual 
 * property is owned by Deeveecube Inc.
 *
 * 2010-2010
 */
package com.deeveecube.reader.library;

import java.util.List;

/**
 * This interface defines general contract for the book library.
 * 
 * @author Dmitry Zhuk
 */
public interface ILibrary {
	/**
	 * Adds single book into the library.
	 * 
	 * @param book
	 *            book to add.
	 * @throws LibraryException
	 *             if addition failed for any reason
	 */
	void add(Book book) throws LibraryException;

	/**
	 * Adds List of books into the library.
	 * 
	 * @param books
	 *            list of books to add
	 * @throws LibraryException
	 *             if addition failed for any reason
	 */
	void add(List<Book> books) throws LibraryException;

	/**
	 * Updates given book in the library.
	 * 
	 * @param book
	 *            book to update.
	 * @throws LibraryException
	 *             if update failed for any reason
	 */
	void update(Book book) throws LibraryException;

	/**
	 * Updates list of books in the library.
	 * 
	 * @param books
	 *            list of books to update.
	 * @throws LibraryException
	 *             if update failed for any reason
	 */
	void update(List<Book> books) throws LibraryException;

	/**
	 * Deletes given book from a library.
	 * 
	 * @param book
	 *            book to delete.
	 * @throws LibraryException
	 *             if deletion failed for any reason
	 */
	void delete(Book book) throws LibraryException;

	/**
	 * Deletes list of books from a library.
	 * 
	 * @param books
	 *            list of books to delete.
	 * @throws LibraryException
	 *             if deletion failed for any reason
	 */
	void delete(List<Book> books) throws LibraryException;

	/**
	 * Lists all books in the library.
	 * <p>
	 * NOTE: This operation can consume significant amount of memory.
	 * 
	 * @return complete list of books in the library.
	 * @throws LibraryException
	 *             if listing failed for any reason
	 */
	List<Book> list() throws LibraryException;

	/**
	 * Lists books of given author.
	 * 
	 * @param author
	 *            An author to list books of.
	 * @return list of books
	 */
	List<Book> list(Author author) throws LibraryException;

	/**
	 * Lists books of given genre.
	 * 
	 * @param genre
	 *            A genre to list books of.
	 * @return list of books.
	 * @throws LibraryException
	 */
	List<Book> list(Genre genre) throws LibraryException;
}
