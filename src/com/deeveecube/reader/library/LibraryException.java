/**
 * This source code and any associated intellectual 
 * property is owned by Deeveecube Inc.
 *
 * 2010-2010
 */
package com.deeveecube.reader.library;

/**
 * @author Dmitry Zhuk
 *
 */
public class LibraryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8233113651741223390L;

	/**
	 * 
	 */
	public LibraryException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param detailMessage
	 */
	public LibraryException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param throwable
	 */
	public LibraryException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param detailMessage
	 * @param throwable
	 */
	public LibraryException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
